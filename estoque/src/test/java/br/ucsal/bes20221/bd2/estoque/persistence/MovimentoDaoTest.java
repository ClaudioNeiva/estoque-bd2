package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.SQLException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.List;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import br.ucsal.bes20221.bd2.estoque.domain.Item;
import br.ucsal.bes20221.bd2.estoque.domain.Movimento;
import br.ucsal.bes20221.bd2.estoque.util.DbTestUtil;

@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class MovimentoDaoTest {

	private ItemDao itemDao = ItemDao.getInstance();
	private MovimentoDao movimentoDao = MovimentoDao.getInstance();

	private Item item1;

	@BeforeAll
	void setupAll() throws SQLException {
		DbTestUtil.deleteTables("movimento", "saldo","item");
		item1 = new Item("parafuso");
		itemDao.insert(item1);
	}

	@BeforeEach
	void setup() throws SQLException {
		DbTestUtil.deleteTables("movimento", "saldo");
	}

	@Test
	void testarInsert() throws SQLException {
		Movimento movimento1 = new Movimento(LocalDate.of(2022, 3, 28), item1, 5, "Compra de parafusos.");
		movimentoDao.insert(movimento1);

		List<Movimento> movimentosEsperados = Arrays.asList(movimento1);
		verificarMovimentos(movimentosEsperados);
	}

	@Test
	void testarDelete() throws SQLException {
		Movimento movimento1 = new Movimento(LocalDate.of(2022, 3, 28), item1, 5, "Compra de parafusos.");
		Movimento movimento2 = new Movimento(LocalDate.of(2022, 3, 29), item1, 25, "Compra de parafusos.");
		movimentoDao.insert(movimento1);
		movimentoDao.insert(movimento2);
		movimentoDao.delete(movimento1);

		List<Movimento> movimentosEsperados = Arrays.asList(movimento2);
		verificarMovimentos(movimentosEsperados);
	}

	@Test
	void testarUpdate() throws SQLException {
		Movimento movimento1 = new Movimento(LocalDate.of(2022, 3, 28), item1, 5, "Compra de parafusos.");
		Movimento movimento2 = new Movimento(LocalDate.of(2022, 3, 29), item1, 25, "Compra de parafusos.");
		movimentoDao.insert(movimento1);
		movimentoDao.insert(movimento2);
		movimento2.setQtd(12);
		movimentoDao.update(movimento2);

		List<Movimento> movimentosEsperados = Arrays.asList(movimento1, movimento2);
		verificarMovimentos(movimentosEsperados);
	}

	private void verificarMovimentos(List<Movimento> movimentosEsperados) throws SQLException {
		List<Movimento> movimentosAtuais = movimentoDao.findAll();
		Assertions.assertEquals(movimentosEsperados, movimentosAtuais);
	}

}

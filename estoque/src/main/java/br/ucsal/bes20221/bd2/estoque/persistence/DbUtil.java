package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

import br.ucsal.bes20221.bd2.estoque.util.PropertyUtil;

public class DbUtil {

	private static Connection connection;

	private DbUtil() {
	}

	public static Connection getConnection() throws SQLException {
		if (connection == null) {
			String url = PropertyUtil.get("database.url");
			String user = PropertyUtil.get("database.user");
			String password = PropertyUtil.get("database.password");
 			connection = DriverManager.getConnection(url, user, password);
		}
		return connection;
	}
}

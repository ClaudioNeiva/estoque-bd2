package br.ucsal.bes20221.bd2.estoque.persistence;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import br.ucsal.bes20221.bd2.estoque.domain.Item;
import br.ucsal.bes20221.bd2.estoque.domain.Saldo;
import br.ucsal.bes20221.bd2.estoque.exception.ApplicationException;

public class SaldoDao extends Dao<Saldo> {

	private static SaldoDao instance;

	public static SaldoDao getInstance() {
		if (instance == null) {
			instance = new SaldoDao();
		}
		return instance;
	}

	private SaldoDao() {
	}

	@Override
	protected String getQuerySelect() {
		return "select s.data, s.id_item as i_id, i.nome as i_nome, s.qtd from saldo s inner join item i on (i.id = s.id_item)";
	}

	@Override
	protected String getQueryInsert() {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected String getQueryUpdate() {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected String getQueryDelete() {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected void setParametersInsert(Saldo object, PreparedStatement stmt) throws SQLException {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected void setParametersUpdate(Saldo object, PreparedStatement stmt) throws SQLException {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected void setParametersDelete(Saldo object, PreparedStatement stmt) throws SQLException {
		throw new ApplicationException("O saldo deve ser controlado pelo trigger.");
	}

	@Override
	protected void setIdObject(Saldo object, PreparedStatement stmt) throws SQLException {
	}

	@Override
	protected Saldo rs2Object(ResultSet rs) throws SQLException {
		Item item = ItemDao.getInstance().rs2Object(rs);
		return new Saldo(rs.getDate("data").toLocalDate(), item, rs.getInt("qtd"));
	}

}
